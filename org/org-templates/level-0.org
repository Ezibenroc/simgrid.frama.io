#+LANGUAGE:  en
#+OPTIONS:   H:3 num:nil toc:nil \n:nil @:t ::t |:t ^:nil -:t f:t *:t <:t
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc
#+OPTIONS:   author:nil email:nil creator:nil timestamp:nil
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport
#+HTML_HEAD:    <link rel="stylesheet" title="Standard" href="css/local.css" type="text/css" />
#+HTML_HEAD:    <link rel="stylesheet" title="Standard" href="css/simgridsite.css" type="text/css" />
#+PROPERTY: header-args :eval never-export

#+BEGIN_EXPORT html
  <div id="header">
    <table width="100%">
      <tr>

        <td width="22%">                        
          <a href="logos/simgrid_logo.pdf">  
            <img src="logos/simgrid_logo_2011.png" width="100%" alt="SimGrid - Simulation of Distributed Computer Systems" />
          </a>
        </td>

        <td valign="middle" align="left">
          <h2 class="header"><i class="subtitle">Simulation of Distributed <br /> Computer Systems</i></h2>
        </td>

        <td valign="middle" align="right">
        <a href="https://lists.gforge.inria.fr/mailman/listinfo/simgrid-user" target="_blank"><img src="./img/mailing_list.png" width="60" alt="mailing list" style="border: 0;"></a>
        </td>

        <td>SimGrid 3.29
          <a href="https://simgrid.org/doc/latest/"><div class="button">User Reference</div></a>
          <a href="https://framagit.org/simgrid/simgrid/uploads/6ca357e80bd4d401bff16367ff1d3dcc/simgrid-3.29.tar.gz">
            <div class="button">Download Source</div></a>
          <a href="https://framagit.org/simgrid/simgrid/uploads/3659eba42deac12db8ae20d1548ffb2c/simgrid-3.29.jar">
            <div class="button">Download Jar</div></a>
          <a href="https://framagit.org/simgrid/simgrid/issues">
            <div class="button">Submit a bug report</div></a>
        </td>
      </tr>
    </table>
  </div>

  <script type="text/javascript">
   var header = "  <div id='navrow1' class='tabs' align='center'>                                                 \n\
    <ul class='tablist'>                                                                                          \n\
      <li class=''><a href='index.html'>Home</a></li>                                                             \n\
      <li class=''><a href='https://simgrid.org/doc/latest/' target='_blank'>Documentation</a></li>               \n\
      <li class=''><a href='publications.html'>Publications</a></li>                                              \n\
      <li class=''><a href='usages.html'>They use SimGrid</a></li>                                                \n\
      <li class=''><a href='devcorner.html'>Devs' Corner</a></li>                                                 \n\
    </ul>                                              \n\
  </div>                                               \n";

  var currentLocation = window.location.toString().split('/').pop().replace(/html#.*$/g,'html');
  //alert(currentLocation);
function menu_item(line) {
      if ((line.replace(/^.*href='/gi, '').replace(/'.*$/gi,'')) == currentLocation) {
 	  return(line.replace(/class=''/,"class='selected'"));
      } else {
	  return(line);
      }
  }

  header.split("\n").map(x => document.write(menu_item(x.replace(/ *$/g,''))));
  </script>

#+END_EXPORT
# The following pagebody is closed in the footer (see init.org)
#+HTML:   <div id="pagebody">

