  (require 'org-install)
  (require 'org)
  (require 'ess-site)
  ;; (require 'org-html)

(setq org-hide-leading-stars t)
(setq org-alphabetical-lists t)
(setq org-src-fontify-natively t)  ;; you want this to activate coloring in blocks
(setq org-src-tab-acts-natively t) ;; you want this to have completion in blocks
(setq org-hide-emphasis-markers t) ;; to hide the *,=, or / markers
(setq org-pretty-entities t)       ;; to have \alpha, \to and others display as utf8 http://orgmode.org/manual/Special-symbols.html

;; (setq org-export-babel-evaluate nil)  ;; :( According to the org-mode9 documentation, when set to nil no code will be evaluated as part of the export process and no header argumentss will be obeyed. Users who wish to avoid evaluating code on export should use the header argument ‘:eval never-export’.
(setq org-confirm-babel-evaluate nil)

(org-babel-do-load-languages
 'org-babel-load-languages
 '(
   (shell . t)
   (python . t)
   (R . t)
   (ruby . t)
   (ocaml . t)
   (ditaa . t)
   (dot . t)
   (octave . t)
   (sqlite . t)
   (perl . t)
   (screen . t)
   (plantuml . t)
   (lilypond . t)
   (makefile . t)
   ))
(setq org-src-preserve-indentation t)

(add-to-list 'org-structure-template-alist
        '("S" "#+begin_src ?\n\n#+end_src" "<src lang=\"?\">\n\n</src>"))

(add-to-list 'org-structure-template-alist
        '("m" "#+begin_src emacs-lisp :tangle init.el \n\n#+end_src" "<src lang=\"emacs-lisp\">\n\n</src>"))

(add-to-list 'org-structure-template-alist
        '("r" "#+begin_src R :results output :session :exports both\n\n#+end_src" "<src lang=\"R\">\n\n</src>"))

(add-to-list 'org-structure-template-alist
        '("R" "#+begin_src R :results output graphics :file (org-babel-temp-file \"figure\" \".png\") :exports both :width 600 :height 400 :session\n\n#+end_src" "<src lang=\"R\">\n\n</src>"))

(add-to-list 'org-structure-template-alist
        '("p" "#+begin_src python :results output raw :exports both\n\n#+end_src" "<src lang=\"python\">\n\n</src>"))

(add-to-list 'org-structure-template-alist
        '("b" "#+begin_src sh :results output :exports both\n\n#+end_src" "<src lang=\"sh\">\n\n</src>"))

(add-to-list 'org-structure-template-alist
        '("B" "#+begin_src sh :session foo :results output :exports both \n\n#+end_src" "<src lang=\"sh\">\n\n</src>"))

(global-set-key (kbd "C-c S-t") 'org-babel-execute-subtree)

(add-hook 'org-babel-after-execute-hook 'org-display-inline-images)
(add-hook 'org-mode-hook 'org-display-inline-images)
(add-hook 'org-mode-hook 'org-babel-result-hide-all)

(setq org-html-htmlize-output-type 'css)

  (setq org-publish-project-alist
        '(
          ("simgrid-html"
           :base-directory "site/"         ; The directory in which the org files for the website reside.
           :base-extension "org"
           :html-extension "html"
           :htmlize-output-type "css"
           :htmlized-source t
           :recursive t
           :base-extension "org"
           :publishing-directory "public_html/"        ; Sets the export directory.
           :publishing-function (org-html-publish-to-html org-org-publish-to-org) ; Tells org to convert the org files to html.
           :html-preamble ""  ; I've put the menu in the org-templates file
           ; The following setting allows to write a common footer to all pages.
           :headline-levels 4             ; Just the default for this project.
           :auto-preamble t
                ; The first </div> closes the opening pagebody div of level-0.org
           :html-postamble "</div>
    <div id='footer' align='center'>
      <p>
        <ul class='footerlogos'>
          <li><a href='http://www.inria.fr/'><img align='center' src='logos/INRIA.png' alt='INRIA'/></a>
          </li>
          <li><a href='http://www.cnrs.fr/'><img align='center' src='logos/cnrs.png' alt='CNRS'/></a>
          </li>
          <li><a href='http://www.ens-rennes.fr/'><img align='center' src='logos/logo-ENSR.png' alt='ENS Rennes'/></a>
          </li>
          <li><a href='http://manoa.hawaii.edu/'><img align='center' src='logos/UnivHawaiiManoa.png' alt='Univeristy of Hawaii Manoa'/></a>
          </li>
        </ul>
      </p>
    </div>
  "
           :style ""                      ; CSS are indicated in the org templates
           :auto-sitemap t                ; Generate sitemap.org automagically...
           :sitemap-filename "sitemap-generated.org"
           :sitemap-title "Sitemap"         ; ... with title 'Sitemap'.
           :sitemap-sort-folders "last"
           :sitemap-ignore-case t
           ;;; tree menus
           ;;;:sitemap-style "tree"
           :exclude "sitemap.org"
           :export-creator-info nil    ; Disable the inclusion of "Created by Org".
           :export-author-info nil     ; Disables the inclusion of "Author: Your Name".
           :auto-postamble nil
           :table-of-contents nil      ; Set this to "t" if you want a table of contents.
           :section-numbers nil        ; Set this to "t" if you want headings to have numbers.
           )
          ("simgrid-static"
           :base-directory "site/"       ; The directory in which the css,jpg,png or pdf files for the website reside.
           :recursive t                                ; Tells org to go through the directory hierarchy.
           :exclude "documentation-experiments"
           :include ("contrib/smpi-saturation-doc/Makefile")
           :base-extension "css\\|jpg\\|png\\|pdf\\|xml\\|csv\\|tgz\\|Rnw\\|c\\|pl\\|sh\\|pcv\\|prv\\|row"     ; Tells org to consider only css,jpg,png and pdf files as being uploaded.
           :publishing-directory "public_html/"      ; Sets the export directory.
           ;:publishing-directory "/ssh:login@server.domain:public_html/" ; Alternative setting, commented out, for publishing on a server.
           :publishing-function org-publish-attachment ; Tells org to just copy these files (css,jpg,png and pdf) around.
           )

          ("simgrid" :components ("simgrid-html" "simgrid-static")) ; If you export project "home", both "simgrid-html" and "simgrid-static" will be exported.

          ))

(global-set-key (kbd "C-c i")
(lambda() (interactive)(org-babel-load-file "./init.org")))

  (defun org-publish-all-custom ()
  ;;   (setq org-export-babel-evaluate nil) ;; do not run babel!
     (org-publish-all t)  ;; passing t to org-publish-all forces to regenerate everything
     (kill-emacs 0)
     )
