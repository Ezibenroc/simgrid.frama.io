#+TITLE:     SimGrid Tutorials
#+SETUPFILE: ../org-templates/level-0.org
#+INCLUDE:   ../org-templates/level-0.org


First grade, first lesson ? Here are the tutorial series! They
constitute a solid piece of information to get into the project, down
to the internal mechanic if you want to. You should probably read at
least the first one to understand the project context and the second
one to get the grasp on the practical aspects of SimGrid.

- [[file:tutorials/simgrid-101.pdf][SimGrid 101]]: General tutorial about SimGrid (Scientific and
  Historical Context of SimGrid; Key Features of the Framework).
- *User Tutorials*
  - [[file:tutorials/simgrid-use-101.pdf][SimGrid and MSG 101]]: Practical introduction to SimGrid and MSG. A
    more recent and up-to-date version of theses slides is also
    available in [[https://simgrid.org/doc/latest/][the main documentation]].
  - [[file:tutorials/simdag-101.pdf][SimDAG 101]]: Practical introduction to the use of SimDag
  - [[file:tutorials/simgrid-platf-101.pdf][Platform 101]]: Defining platforms and experiments in SimGrid
  - [[file:tutorials/simgrid-energy-101.pdf][Energy/DVFS 101]]: Energize your publications with SimGrid ;)
  - [[file:tutorials/simgrid-smpi-101.pdf][SMPI 101]]: Simulation MPI applications in practice. We strongly
    recommand to have a look at the following [[https://simgrid.github.io/SMPI_CourseWare/][courseware]] that we
    regularly use with our own students.
  - [[file:tutorials/simgrid-tracing-101.pdf][Visualization 101]]: Visualization and analysis of SimGrid
    simulation results. This set of slides is a bit outdated. Instead,
    we strongly recommand you to have a look the [[file:contrib/R_visualization.org][dedicated contrib
    section]] that explains how to perform similar visualizations using
    R scripts.
  - [[file:tutorials/simgrid-mc-101.pdf][Model-checking 101]]: Formal Verification of SimGrid programs
  - [[file:tutorials/simgrid-io-ccgrid.pdf][Simulating Storage]]: Presentation at CCGrid 2015 of the basis of
    storage modeling in SimGrid
- *Tutorials on the internals*:
  - [[file:tutorials/surf-101.pdf][SimGrid Internal::Models 101]]: The Platform Models underlying SimGrid
  - [[file:tutorials/simgrid-simix-101.pdf][SimGrid Internal::Kernel 101]]: Under the Hood of SimGrid

