Analysis of Pont-to-point experiments of MPI calls
==================================================

```r
opts_chunk$set(cache=FALSE,dpi=300,echo=FALSE)
```


If needed, you should install the right packages (plyr, ggplot2, and
knitr) with the install.packages command.

```
## Loading required package: plyr
## Loading required package: ggplot2
## Loading required package: methods
## Loading required package: XML
```
Load XML config file and .csv resulting files from the MPI execution



MPI_Send timing
---------------

Timings for this experiment are taken from a ping-pong experiment, used to determine os.

We determine the piecewiese regression based on information taken from the regression file pointed in the XML configuration file


```
##        Limit         Name LimitInf
## 1       4120       Medium        0
## 2      17408 Asynchronous     4120
## 3 2147483647        Large    17408
```
Display the regression factors to help tuning.

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -4.309e-07 -1.088e-07 -7.530e-08  1.401e-07  3.389e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 3.338e-07  1.144e-09  291.90   <2e-16 ***
## Size        1.198e-10  1.205e-12   99.39   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.803e-07 on 32225 degrees of freedom
## Multiple R-squared:  0.2346,	Adjusted R-squared:  0.2346 
## F-statistic:  9878 on 1 and 32225 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -8.075e-07 -4.425e-07 -2.409e-07  2.969e-07  7.409e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.800e-06  2.161e-08   83.30   <2e-16 ***
## Size        1.378e-10  2.176e-12   63.35   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.191e-07 on 6089 degrees of freedom
## Multiple R-squared:  0.3973,	Adjusted R-squared:  0.3972 
## F-statistic:  4014 on 1 and 6089 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value.

The black line representing the regression should be very close to the values, and should drop to 0 when communications use the rendez-vous algorithm (Large messages, with a size > eager_threshold).

If they are not, tune the breakpoints in order to match more closely to your implementation. Thresholds for eager and detached messages depend on the library and the hardware used. Consult the documentation of your library on how to display this information if you can't visually determine it (For Ethernet network we saw values of 65536, while IB networks had values of 12288 or 17408 depending on the implementation)


```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6-1.png) 
MPI_Isend timing
---------------

As they may differ from Send times, check this and call it ois, to inject proper timings later.


Display the regression factors to help tuning

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -4.403e-07 -1.023e-07 -6.920e-08  9.900e-08  4.236e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 2.996e-07  9.094e-10   329.4   <2e-16 ***
## Size        1.313e-10  9.610e-13   136.6   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.435e-07 on 32291 degrees of freedom
## Multiple R-squared:  0.3663,	Adjusted R-squared:  0.3663 
## F-statistic: 1.867e+04 on 1 and 32291 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.808e-07 -1.042e-07 -7.874e-08  4.741e-08  1.309e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 5.226e-07  5.791e-09  90.248   <2e-16 ***
## Size        5.593e-12  5.828e-13   9.596   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.658e-07 on 6087 degrees of freedom
## Multiple R-squared:  0.0149,	Adjusted R-squared:  0.01474 
## F-statistic: 92.08 on 1 and 6087 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-9](figure/unnamed-chunk-9-1.png) 

MPI_Recv timing
---------------

Timings are used to determine or. This experiment waits for a potentially eager message to arrive before launching the recv for small message size, eliminating waiting times as much as possible.


Display the regression factors to help tuning

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -3.804e-07 -1.767e-07 -2.940e-08  1.420e-08  1.935e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.364e-06  2.343e-09  582.09   <2e-16 ***
## Size        1.080e-10  2.473e-12   43.67   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 3.695e-07 on 32231 degrees of freedom
## Multiple R-squared:  0.05586,	Adjusted R-squared:  0.05583 
## F-statistic:  1907 on 1 and 32231 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-12](figure/unnamed-chunk-12-1.png) 

Pingpong timing
---------------

pingpong = 2or+2transfer for small messages that are sent
  asynchronously.  For large sizes, communications are synchronous,
  hence we have pingpong = 2transfer.


```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.101e-06 -4.426e-07 -8.910e-08  3.324e-07  2.352e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.373e-06  3.348e-09   410.0   <2e-16 ***
## Size        3.878e-10  3.534e-12   109.7   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 5.889e-07 on 40098 degrees of freedom
## Multiple R-squared:  0.231,	Adjusted R-squared:  0.2309 
## F-statistic: 1.204e+04 on 1 and 40098 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.484e-06 -8.347e-07 -5.210e-07  4.146e-07  2.951e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 2.999e-06  4.061e-08   73.85   <2e-16 ***
## Size        2.840e-10  4.088e-12   69.48   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.299e-06 on 7598 degrees of freedom
## Multiple R-squared:  0.3885,	Adjusted R-squared:  0.3885 
## F-statistic:  4828 on 1 and 7598 DF,  p-value: < 2.2e-16
## 
## [1] "----- Large-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -2.639e-05 -2.599e-06 -8.210e-07  7.440e-07  1.581e-04 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 5.210e-06  7.068e-08   73.71   <2e-16 ***
## Size        2.159e-10  1.975e-13 1093.16   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.993e-06 on 18498 degrees of freedom
## Multiple R-squared:  0.9848,	Adjusted R-squared:  0.9848 
## F-statistic: 1.195e+06 on 1 and 18498 DF,  p-value: < 2.2e-16
```

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-1.png) 

```
## Warning in scale$trans$trans(x): NaNs produced
```

```
## Warning: Removed 18212 rows containing missing values (geom_point).
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-2.png) 

Print results in Simgrid's xml format


MPI_Wtime timing
---------------

We made a run with 10 millions calls to MPI\_Wtime and we want to know the time of one call


Time for one MPI_Wtime call

```
## [1] 3.54637e-08
```
MPI_Iprobe timing
----------------
We made 1000 runs of pingpong with pollling on MPI\_Iprobe. Compute the Duration of such a call, and check whether its time is related to the size of the message

![plot of chunk unnamed-chunk-17](figure/unnamed-chunk-17-1.png) 
Time for one MPI_Iprobe call

```
## [1] 4.441786e-07
```

MPI_Test timing
---------------

![plot of chunk unnamed-chunk-19](figure/unnamed-chunk-19-1.png) 
Time for one MPI_Test call

```
## [1] 6.735357e-07
```


Result of calibration.
---------------

The following snippet of XML has to be included at the beginning of your platformfile. Please report to the SimGrid mailing list any bug with the calibration or the generated platform file.


```
## <config id="General">
##  <prop id="smpi/os" value="0:3.33809800772755e-07:1.19758012561479e-10;4120:1.80025892006293e-06:1.37844482790579e-10;17408:0:0"/>
##  <prop id="smpi/ois" value="0:2.9956039499098e-07:1.31295276288599e-10;4120:5.22643731028535e-07:5.59286363481658e-12;17408:0:0"/>
##  <prop id="smpi/or" value="0:1.3635950877903e-06:1.08008992233296e-10;4120:0:0;17408:0:0"/>
##  <prop id="smpi/bw_factor" value="0:1.0212271015512;4120:1.00592739101783;17408:1.32354721253027"/>
##  <prop id="smpi/lat_factor" value="0:0.000453157054255973;4120:0.149947156921692;17408:0.260476437970144"/>
##  <prop id="smpi/async_small_thres" value="17408"/>
##  <prop id="smpi/send_is_detached_thres" value="17408"/>
##  <prop id="smpi/wtime" value="3.54637e-08"/>
##  <prop id="smpi/iprobe" value="4.44178571428571e-07"/>
##  <prop id="smpi/test" value="6.73535714285714e-07"/>
## </config>
```

```
## [1] "Results written in stampede.xml"
```

```
## [1] "stampede_output.xml"
```

