Analysis of Pont-to-point experiments of MPI calls
==================================================

```r
opts_chunk$set(cache=FALSE,dpi=300,echo=FALSE)
```


If needed, you should install the right packages (plyr, ggplot2, and
knitr) with the install.packages command.

```
## Loading required package: plyr
## Loading required package: ggplot2
## Loading required package: methods
## Loading required package: XML
```
Load XML config file and .csv resulting files from the MPI execution



MPI_Send timing
---------------

Timings for this experiment are taken from a ping-pong experiment, used to determine os.

We determine the piecewiese regression based on information taken from the regression file pointed in the XML configuration file


```
##        Limit         Name LimitInf
## 1       4020       Medium        0
## 2      17408 Asynchronous     4020
## 3 2147483647        Large    17408
```
Display the regression factors to help tuning.

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -4.907e-07 -1.652e-07  4.156e-08  1.093e-07  7.158e-07 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 4.301e-07  1.011e-09  425.46   <2e-16 ***
## Size        1.050e-10  1.092e-12   96.22   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.59e-07 on 32100 degrees of freedom
## Multiple R-squared:  0.2239,	Adjusted R-squared:  0.2238 
## F-statistic:  9259 on 1 and 32100 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.037e-06 -6.170e-07 -2.068e-07  4.870e-07  2.826e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 2.003e-06  2.484e-08   80.63   <2e-16 ***
## Size        1.320e-10  2.514e-12   52.49   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 7.241e-07 on 6163 degrees of freedom
## Multiple R-squared:  0.3089,	Adjusted R-squared:  0.3088 
## F-statistic:  2755 on 1 and 6163 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value.

The black line representing the regression should be very close to the values, and should drop to 0 when communications use the rendez-vous algorithm (Large messages, with a size > eager_threshold).

If they are not, tune the breakpoints in order to match more closely to your implementation. Thresholds for eager and detached messages depend on the library and the hardware used. Consult the documentation of your library on how to display this information if you can't visually determine it (For Ethernet network we saw values of 65536, while IB networks had values of 12288 or 17408 depending on the implementation)


```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6-1.png) 
MPI_Isend timing
---------------

As they may differ from Send times, check this and call it ois, to inject proper timings later.


Display the regression factors to help tuning

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -3.182e-07 -7.742e-08 -3.822e-08  4.845e-08  9.570e-07 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 2.571e-07  6.963e-10   369.2   <2e-16 ***
## Size        1.331e-10  7.513e-13   177.1   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.097e-07 on 32232 degrees of freedom
## Multiple R-squared:  0.4933,	Adjusted R-squared:  0.4933 
## F-statistic: 3.138e+04 on 1 and 32232 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.879e-07 -1.084e-07 -5.454e-08  7.580e-08  8.882e-07 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 4.929e-07  4.918e-09  100.23   <2e-16 ***
## Size        5.660e-12  4.977e-13   11.37   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.436e-07 on 6181 degrees of freedom
## Multiple R-squared:  0.02049,	Adjusted R-squared:  0.02033 
## F-statistic: 129.3 on 1 and 6181 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-9](figure/unnamed-chunk-9-1.png) 

MPI_Recv timing
---------------

Timings are used to determine or. This experiment waits for a potentially eager message to arrive before launching the recv for small message size, eliminating waiting times as much as possible.


Display the regression factors to help tuning

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -3.547e-07 -8.308e-08 -2.410e-09  4.094e-08  2.229e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.325e-06  1.323e-09 1001.58   <2e-16 ***
## Size        1.044e-10  1.429e-12   73.07   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 2.081e-07 on 32108 degrees of freedom
## Multiple R-squared:  0.1426,	Adjusted R-squared:  0.1426 
## F-statistic:  5339 on 1 and 32108 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-12](figure/unnamed-chunk-12-1.png) 

Pingpong timing
---------------

pingpong = 2or+2transfer for small messages that are sent
  asynchronously.  For large sizes, communications are synchronous,
  hence we have pingpong = 2transfer.


```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -9.956e-07 -3.967e-07  1.288e-07  2.969e-07  9.879e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.531e-06  2.610e-09   586.6   <2e-16 ***
## Size        3.243e-10  2.817e-12   115.1   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 4.582e-07 on 39998 degrees of freedom
## Multiple R-squared:  0.249,	Adjusted R-squared:  0.2489 
## F-statistic: 1.326e+04 on 1 and 39998 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.701e-06 -1.074e-06 -5.147e-07  1.153e-06  1.535e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 3.309e-06  3.788e-08   87.37   <2e-16 ***
## Size        2.656e-10  3.834e-12   69.28   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.234e-06 on 7698 degrees of freedom
## Multiple R-squared:  0.3841,	Adjusted R-squared:  0.384 
## F-statistic:  4800 on 1 and 7698 DF,  p-value: < 2.2e-16
## 
## [1] "----- Large-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.432e-05 -1.338e-06 -5.870e-07  3.560e-07  3.752e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 5.988e-06  2.566e-08   233.4   <2e-16 ***
## Size        2.015e-10  7.169e-14  2811.0   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 2.539e-06 on 18498 degrees of freedom
## Multiple R-squared:  0.9977,	Adjusted R-squared:  0.9977 
## F-statistic: 7.902e+06 on 1 and 18498 DF,  p-value: < 2.2e-16
```

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-1.png) 

```
## Warning in scale$trans$trans(x): NaNs produced
```

```
## Warning: Removed 10634 rows containing missing values (geom_point).
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-2.png) 

Print results in Simgrid's xml format


MPI_Wtime timing
---------------

We made a run with 10 millions calls to MPI\_Wtime and we want to know the time of one call


Time for one MPI_Wtime call

```
## [1] 3.807735e-08
```
MPI_Iprobe timing
----------------
We made 1000 runs of pingpong with pollling on MPI\_Iprobe. Compute the Duration of such a call, and check whether its time is related to the size of the message

![plot of chunk unnamed-chunk-17](figure/unnamed-chunk-17-1.png) 
Time for one MPI_Iprobe call

```
## [1] 4.56844e-07
```

MPI_Test timing
---------------

![plot of chunk unnamed-chunk-19](figure/unnamed-chunk-19-1.png) 
Time for one MPI_Test call

```
## [1] 6.399524e-07
```


Result of calibration.
---------------

The following snippet of XML has to be included at the beginning of your platformfile. Please report to the SimGrid mailing list any bug with the calibration or the generated platform file.


```
## <config id="General">
##  <prop id="smpi/os" value="0:4.30128761160071e-07:1.05027825903452e-10;4020:2.00263206389841e-06:1.31954349276849e-10;17408:0:0"/>
##  <prop id="smpi/ois" value="0:2.57094359224223e-07:1.33084988946133e-10;4020:4.92914493858109e-07:5.65952114026689e-12;17408:0:0"/>
##  <prop id="smpi/or" value="0:1.32499403399468e-06:1.04394238244888e-10;4020:0:0;17408:0:0"/>
##  <prop id="smpi/bw_factor" value="0:1.29900018920374;4020:1.07571182189868;17408:1.41769507997096"/>
##  <prop id="smpi/lat_factor" value="0:0.0102961468300213;4020:0.16546914117845;17408:0.299398222975123"/>
##  <prop id="smpi/async_small_thres" value="17408"/>
##  <prop id="smpi/send_is_detached_thres" value="17408"/>
##  <prop id="smpi/wtime" value="3.807735e-08"/>
##  <prop id="smpi/iprobe" value="4.56844036697248e-07"/>
##  <prop id="smpi/test" value="6.39952380952381e-07"/>
## </config>
```

```
## [1] "Results written in laptop.xml"
```

```
## [1] "laptop_output.xml"
```

