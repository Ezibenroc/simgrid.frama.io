Analysis of Pont-to-point experiments of MPI calls
==================================================

```r
opts_chunk$set(cache=FALSE,dpi=300,echo=FALSE)
```


If needed, you should install the right packages (plyr, ggplot2, and
knitr) with the install.packages command.

```
## Loading required package: plyr
## Loading required package: ggplot2
## Loading required package: methods
## Loading required package: XML
```
Load XML config file and .csv resulting files from the MPI execution



MPI_Send timing
---------------

Timings for this experiment are taken from a ping-pong experiment, used to determine os.

We determine the piecewiese regression based on information taken from the regression file pointed in the XML configuration file


```
##        Limit         Name LimitInf
## 1       4020       Medium        0
## 2      17408 Asynchronous     4020
## 3 2147483647        Large    17408
```
Display the regression factors to help tuning.

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -4.241e-07 -1.654e-07 -6.020e-08  1.562e-07  8.631e-07 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 3.642e-07  1.137e-09  320.23   <2e-16 ***
## Size        1.036e-10  1.228e-12   84.38   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.79e-07 on 32148 degrees of freedom
## Multiple R-squared:  0.1813,	Adjusted R-squared:  0.1813 
## F-statistic:  7120 on 1 and 32148 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.000e-06 -5.689e-07 -1.398e-07  3.997e-07  2.499e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.974e-06  2.292e-08   86.14   <2e-16 ***
## Size        1.386e-10  2.319e-12   59.76   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 6.683e-07 on 6167 degrees of freedom
## Multiple R-squared:  0.3667,	Adjusted R-squared:  0.3666 
## F-statistic:  3571 on 1 and 6167 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value.

The black line representing the regression should be very close to the values, and should drop to 0 when communications use the rendez-vous algorithm (Large messages, with a size > eager_threshold).

If they are not, tune the breakpoints in order to match more closely to your implementation. Thresholds for eager and detached messages depend on the library and the hardware used. Consult the documentation of your library on how to display this information if you can't visually determine it (For Ethernet network we saw values of 65536, while IB networks had values of 12288 or 17408 depending on the implementation)


```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6-1.png) 
MPI_Isend timing
---------------

As they may differ from Send times, check this and call it ois, to inject proper timings later.


Display the regression factors to help tuning

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -2.587e-07 -4.900e-08 -2.740e-08  1.220e-08  3.598e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 2.263e-07  5.615e-10   403.0   <2e-16 ***
## Size        1.257e-10  6.062e-13   207.3   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 8.85e-08 on 32233 degrees of freedom
## Multiple R-squared:  0.5713,	Adjusted R-squared:  0.5713 
## F-statistic: 4.296e+04 on 1 and 32233 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.431e-07 -8.593e-08 -5.645e-08  1.406e-08  1.541e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 4.562e-07  4.850e-09   94.05   <2e-16 ***
## Size        6.591e-12  4.909e-13   13.43   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.415e-07 on 6172 degrees of freedom
## Multiple R-squared:  0.02838,	Adjusted R-squared:  0.02822 
## F-statistic: 180.3 on 1 and 6172 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-9](figure/unnamed-chunk-9-1.png) 

MPI_Recv timing
---------------

Timings are used to determine or. This experiment waits for a potentially eager message to arrive before launching the recv for small message size, eliminating waiting times as much as possible.


Display the regression factors to help tuning

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -3.818e-07 -2.021e-07 -1.746e-07 -1.095e-07  3.056e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.299e-06  2.750e-09  472.14   <2e-16 ***
## Size        1.485e-10  2.969e-12   50.01   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 4.33e-07 on 32149 degrees of freedom
## Multiple R-squared:  0.07219,	Adjusted R-squared:  0.07216 
## F-statistic:  2501 on 1 and 32149 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-12](figure/unnamed-chunk-12-1.png) 

Pingpong timing
---------------

pingpong = 2or+2transfer for small messages that are sent
  asynchronously.  For large sizes, communications are synchronous,
  hence we have pingpong = 2transfer.


```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.163e-06 -5.122e-07 -3.960e-08  4.228e-07  1.524e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.409e-06  3.117e-09   452.0   <2e-16 ***
## Size        3.885e-10  3.364e-12   115.5   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 5.473e-07 on 39998 degrees of freedom
## Multiple R-squared:   0.25,	Adjusted R-squared:   0.25 
## F-statistic: 1.333e+04 on 1 and 39998 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.747e-06 -1.058e-06 -2.739e-07  9.607e-07  1.309e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 3.273e-06  3.881e-08   84.34   <2e-16 ***
## Size        2.841e-10  3.928e-12   72.34   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.265e-06 on 7698 degrees of freedom
## Multiple R-squared:  0.4047,	Adjusted R-squared:  0.4046 
## F-statistic:  5233 on 1 and 7698 DF,  p-value: < 2.2e-16
## 
## [1] "----- Large-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.722e-05 -2.241e-06 -9.790e-07  6.420e-07  5.488e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 6.019e-06  4.155e-08   144.9   <2e-16 ***
## Size        2.095e-10  1.161e-13  1805.2   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 4.11e-06 on 18498 degrees of freedom
## Multiple R-squared:  0.9944,	Adjusted R-squared:  0.9944 
## F-statistic: 3.259e+06 on 1 and 18498 DF,  p-value: < 2.2e-16
```

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-1.png) 

```
## Warning in scale$trans$trans(x): NaNs produced
```

```
## Warning: Removed 16449 rows containing missing values (geom_point).
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-2.png) 

Print results in Simgrid's xml format


MPI_Wtime timing
---------------

We made a run with 10 millions calls to MPI\_Wtime and we want to know the time of one call


Time for one MPI_Wtime call

```
## [1] 3.645389e-08
```
MPI_Iprobe timing
----------------
We made 1000 runs of pingpong with pollling on MPI\_Iprobe. Compute the Duration of such a call, and check whether its time is related to the size of the message

![plot of chunk unnamed-chunk-17](figure/unnamed-chunk-17-1.png) 
Time for one MPI_Iprobe call

```
## [1] 4.510357e-07
```

MPI_Test timing
---------------

![plot of chunk unnamed-chunk-19](figure/unnamed-chunk-19-1.png) 
Time for one MPI_Test call

```
## [1] 6.614068e-07
```


Result of calibration.
---------------

The following snippet of XML has to be included at the beginning of your platformfile. Please report to the SimGrid mailing list any bug with the calibration or the generated platform file.


```
## <config id="General">
##  <prop id="smpi/os" value="0:3.64193933074989e-07:1.03595324447199e-10;4020:1.97385572038802e-06:1.38592474231191e-10;17408:0:0"/>
##  <prop id="smpi/ois" value="0:2.26303765145653e-07:1.25650801588468e-10;4020:4.56182283826057e-07:6.59064774739268e-12;17408:0:0"/>
##  <prop id="smpi/or" value="0:1.29859096043907e-06:1.48511002580468e-10;4020:0:0;17408:0:0"/>
##  <prop id="smpi/bw_factor" value="0:1.19077317119758;4020:1.00558721702881;17408:1.36349811028625"/>
##  <prop id="smpi/lat_factor" value="0:0.0055296220665081;4020:0.163662116133325;17408:0.300927664785292"/>
##  <prop id="smpi/async_small_thres" value="17408"/>
##  <prop id="smpi/send_is_detached_thres" value="17408"/>
##  <prop id="smpi/wtime" value="3.645389e-08"/>
##  <prop id="smpi/iprobe" value="4.51035714285714e-07"/>
##  <prop id="smpi/test" value="6.61406779661017e-07"/>
## </config>
```

```
## [1] "Results written in laptop_2nd_experiment.xml"
```

```
## [1] "laptop_2nd_experiment_output.xml"
```

