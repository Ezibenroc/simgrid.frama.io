#!/bin/bash

#
# Simple wrapper for SMPI based on the Dimemas one
#

set -e

function usage
{
  echo "Usage: $0  source_trace  dimemas_cfg  output_trace  reuse_dimemas_trace [extra_parameters] [-n]"
  echo "  source_trace:        Paraver trace"
  echo "  dimemas_cfg:         Simulation parameters"
  echo "  output_trace:        Output trace of Dimemas; must end with '.prv'"
  echo "  reuse_dimemas_trace: 0 -> don't reuse, rerun prv2dim"
  echo "                       1 -> reuse, don't rerun prv2dim"
  echo "  extra_parameters:    See complete list of Dimemas help with 'Dimemas -h'"
  echo "  -n:                  prv2dim -n parameter => no generate initial idle states"
}


# Read and check parameters
if [ $# -lt 4 ]; then
  usage
  exit 1
fi

#PARAVER_TRACE=${1}
PARAVER_TRACE=`readlink -eqs "${1}"`
DIMEMAS_CFG=${2}
OUTPUT_PARAVER_TRACE=${3}
DIMEMAS_REUSE_TRACE=${4}


if [[ ${DIMEMAS_REUSE_TRACE} != "0"  && ${DIMEMAS_REUSE_TRACE} != "1" ]]; then
  usage
  exit 1
fi

echo "===============================================================================" 

# Check SMPI availability
### Oh right, we should do that...

# Get tracename, without extensions
TRACENAME=$(echo "$PARAVER_TRACE" | sed "s/\.[^\.]*$//")
EXTENSION=$(echo "$PARAVER_TRACE" | sed "s/^.*\.//")

#Is gzipped?
if [[ ${EXTENSION} = "gz" ]]; then
  echo
  echo -n "[MSG] Decompressing $PARAVER_TRACE trace..."
  gunzip ${PARAVER_TRACE}
  TRACENAME=$(echo "${TRACENAME}" | sed "s/\.[^\.]*$//")
  PARAVER_TRACE=${TRACENAME}.prv
  echo "...Done!"
fi

DIMEMAS_TRACE=${TRACENAME}.dim

# Adapt Dimemas CFG with new trace name
DIMEMAS_CFG_NAME=$(echo "$DIMEMAS_CFG" | sed "s/\.[^\.]*$//")

DIMEMAS_COPY_CFG_NAME=`basename ${DIMEMAS_CFG_NAME}`
OLD_DIMEMAS_TRACENAME=`grep "mapping information" ${DIMEMAS_CFG} | grep ".dim" | awk -F'"' {'print $4'}`
NEW_DIMEMAS_TRACENAME=`basename ${DIMEMAS_TRACE}`
DIMEMAS_CFG_PATH=`dirname ${DIMEMAS_TRACE}`

# Append extra parameters if they exist
shift
shift
shift
shift
EXTRA_PARAMETERS=""
PRV2DIM_N=""
while [ -n "$1" ]; do
  if [[ ${1} == "-n" ]]; then # caution! this works because no -n parameters exists in Dimemas
    PRV2DIM_N="-n"
  else
    EXTRA_PARAMETERS="$EXTRA_PARAMETERS $1"
  fi
  shift
done

# Change directory to see .dim
DIMEMAS_TRACE_DIR=`dirname ${DIMEMAS_TRACE}`/
pushd . > /dev/null
cd ${DIMEMAS_TRACE_DIR}


# Translate from .prv to SMPI time independant trace

if [[ ${DIMEMAS_REUSE_TRACE} = "0" || \
      ${DIMEMAS_REUSE_TRACE} = "1" && ! -f ${DIMEMAS_TRACE} ]]; then

  if [[ ${DIMEMAS_REUSE_TRACE} = "1" ]]; then
    echo
    echo "[WARN] Unable to find ${DIMEMAS_TRACE}"
    echo "[WARN] Generating it."
  fi

  PARAVER_TRACE_TRIMED=`echo ${PARAVER_TRACE} | sed 's/.prv$//'`
  echo
  echo "[COM] prv2pj.pl -i ${PARAVER_TRACE_TRIMED} -o ${DIMEMAS_TRACE} -f tit"
  echo
  prv2pj.pl -i ${PARAVER_TRACE_TRIMED} -o ${DIMEMAS_TRACE} -f tit
  echo
fi


# Simulate
OUTPUT_PAJE_TRACE=`echo ${OUTPUT_PARAVER_TRACE} | sed 's/.prv$/.trace/'`
  echo 
  echo "*** Running SMPI :) ***"
  echo 
  echo "[COM]   smpi2pj.sh -i ${DIMEMAS_TRACE} -o ${OUTPUT_PAJE_TRACE}"
  echo
smpi2pj.sh -i ${DIMEMAS_TRACE} -o ${OUTPUT_PAJE_TRACE}

# Convert back to paraver
  echo "*** Converting to Paraver ***"
  echo 
  echo "[COM]   pjdump2prv.pl -i ${OUTPUT_PAJE_TRACE} -o ${OUTPUT_PARAVER_TRACE} -p `which pj_dump`"
  echo
pjdump2prv.pl -i ${OUTPUT_PAJE_TRACE} -o ${OUTPUT_PARAVER_TRACE}
echo "===============================================================================" 

popd > /dev/null
